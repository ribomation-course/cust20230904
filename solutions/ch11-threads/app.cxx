#include <iostream>
#include <thread>
#include <vector>
#include <syncstream>
using namespace std::chrono_literals;

int main() {
    std::cout << "[main] enter\n";
    auto const N = std::thread::hardware_concurrency();
    {
        auto threads = std::vector<std::jthread>{};
        threads.reserve(N);
        for (auto k = 1U; k <= N; ++k) {
            threads.emplace_back([](unsigned id){
                std::osyncstream{std::cout} << "THR-" << id << " enter\n";
                std::this_thread::sleep_for(1s);
                std::osyncstream{std::cout} << "THR-" << id << " exit\n";
            }, k);
        }
    }
    std::cout << "[main] exit\n";
}
