#include <iostream>
#include <iomanip>
#include <thread>
#include <syncstream>
#include "message-queue.hxx"

using namespace std::literals;
namespace rm = ribomation::threads;

struct Producer {
    const unsigned max;
    rm::MessageQueue<unsigned>& out;

    Producer(unsigned max, rm::MessageQueue<unsigned>& out) : max(max), out(out) {}

    void operator()() {
        for (auto k = 1U; k <= max; ++k) out.put(k);
        out.put(0);
        std::osyncstream{std::cout} << "P: done\n";
    }
};

struct Consumer {
    rm::MessageQueue<unsigned>& in;

    explicit Consumer(rm::MessageQueue<unsigned>& in) : in(in) {}

    void operator()() {
        for (auto msg = in.get(); msg != 0; msg = in.get()) {
            std::osyncstream{std::cout} << "C: " << std::setw(6) << msg << "\n";
        }
        std::osyncstream{std::cout} << "C: done\n";
    }
};

int main() {
    auto N = 1'000U;
    auto Q = rm::MessageQueue < unsigned > {16};
    auto cons = std::jthread{Consumer{Q}};
    auto prod = std::jthread{Producer{N, Q}};
}
