#include <iostream>
#include <algorithm>
#include <functional>
using std::cout;

auto count_if(int* first, int const* last, std::function<bool(int)> const& pred) {
    auto cnt = 0U;
    for (; first != last; ++first) {
        if (pred(*first)) ++cnt;
    }
    return cnt;
}

int main() {
    {
        int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        auto const N = sizeof(numbers) / sizeof(numbers[0]);

        auto pr = [](int n) { cout << n << " "; };
        std::for_each(numbers, numbers + N, pr);
        cout << "\n";
        auto factor = 42;
        std::transform(numbers, numbers + N, numbers, [factor](int n) { return factor * n; });
        std::for_each(numbers, numbers + N, pr);
        cout << "\n";
    }

    {
        int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        auto const N = sizeof(numbers) / sizeof(numbers[0]);
        auto result = count_if(numbers, numbers + N, [](int n) -> bool { return n % 3 == 0; });
        cout << "result: " << result << "\n";
    }
}


