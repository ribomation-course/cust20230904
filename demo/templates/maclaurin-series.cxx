#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;
using namespace std::literals;

template<typename T>
constexpr T POW(T x, unsigned N) {
    if (N == 0U) return 1;
    if (N == 1U) return x;
    return x * POW(x, N - 1);
}

constexpr unsigned FAC(unsigned N) {
    if (N == 0U) return 0;
    if (N == 1U) return 1;
    return N * FAC(N - 1);
}

constexpr double sine_term(double x, unsigned N) {
    return POW(-1, N) * POW(x, 2 * N + 1) / FAC(2 * N + 1);
}

constexpr double sine(double x, unsigned N) {
    if (N == 0) return sine_term(x, 0);
    return sine_term(x, N) + sine(x, N - 1);
}


int main() {
    unsigned  values[] = {POW(2U, 10), FAC(5U)};
    for (auto n : values) cout << n << " ";
    cout << endl;

    constexpr double x       = 0.5*M_PI;
    constexpr double terms[] = {sine_term(x, 0), sine_term(x, 1), sine_term(x, 2), sine_term(x, 3)};
    for (auto        n : terms) cout << n << " ";
    cout << endl;

    constexpr double sine_05 = sine(x, 10);
    cout << fixed << setprecision(10) << "sine(x) = " << sine_05 << endl;
    cout << fixed << setprecision(10) << "sin(x)  = " << sin(x) << endl;
    cout << fixed << setprecision(10) << "diff    = " << abs(sin(x) - sine_05) << endl;

    return 0;
}
