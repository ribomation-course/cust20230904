#pragma once

#include <string>

namespace ribomation::shm {

    class SharedMemory {
        const std::string name;
        const unsigned    size;
        const void* begin;
        const void* end;
        void      * next;

    public:
        explicit SharedMemory(size_t size_, void* addr_ = 0, const std::string& name_ = "/shmxx");
        ~SharedMemory();

        SharedMemory(SharedMemory&&) noexcept;

        SharedMemory& operator=(SharedMemory&&) noexcept;

        [[nodiscard]] size_t capacity() const { return size; }

        [[nodiscard]] size_t free() const {
            return (reinterpret_cast<size_t>(end) - reinterpret_cast<size_t>(next));
        }

        [[nodiscard]] void* allocateBytes(size_t num_bytes);
        
        template<typename ObjType>
        [[nodiscard]] ObjType* allocate(unsigned n = 1) {
            return reinterpret_cast<ObjType*>(allocateBytes(n * sizeof(ObjType)));
        }

        SharedMemory() = delete;
        SharedMemory(const SharedMemory&) = delete;
        SharedMemory& operator=(const SharedMemory&) = delete;
    };


}


