#include <iostream>

using namespace std;
using namespace std::literals;

template<typename ... T>
auto SUM(T ... args) {
    return (... +args);
}

int main() {
    cout << "numb: " << SUM(1, 2, 3, 4, 5) << endl;
    cout << "text: " << SUM("Hi"s, "-"s, "Fi"s) << endl;

    auto s1 = "Foo"s;
    auto s2 = "Boo"s;
    cout << "vars: " << SUM(s1, s2) << endl;
    return 0;
}


