#include <iostream>
#include "person.hxx"
namespace rm = ribomation;

void doit(rm::Person p) {
    std::cout << "[doit] " << p << "\n";
}

int main() {
    std::cout << "[main] --- enter ---\n";
    {
        auto anna = rm::Person{"Anna Conda", 42};
        std::cout << "[main] " << anna << "\n";
        doit(std::move(anna));  // (4)
        std::cout << "[main] " << anna << "\n";
    }
    std::cout << "[main] --- exit ---\n";
}
