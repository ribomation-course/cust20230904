#include <iostream>

constexpr auto factorial(unsigned n) {
    if (n == 0) return 0UL;
    if (n == 1) return 1UL;
    auto result = 1UL;
    for (; n > 1; --n) result *= n;
    return result;
}

int main() {
    constexpr unsigned long values[] = {
            factorial(2),
            factorial(5),
            factorial(10),
            factorial(15),
            factorial(20),
    };
    for (auto v: values)
        std::cout << std::dec << v
                  << " (" << std::uppercase << std::hex << v << ")\n";
}
