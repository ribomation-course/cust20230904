#include <iostream>
#include <string>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <cctype>

int main() {
    auto words = std::unordered_multiset<std::string>{};
    for (std::string w; std::cin >> w;) {
        auto last = std::remove_if(w.begin(), w.end(), [](char ch){return not ::isalpha(ch);});
        w.erase(last, w.end());
        if (not w.empty()) words.insert(w);
    }

    auto words2 = std::multiset<std::string>{words.begin(), words.end()};
    for (auto it = words2.begin(); it != words2.end();) {
        auto word = *it;
        auto count = words2.count(word);
        std::advance(it, count); /*it += count;*/
        std::cout << word << ": " << count << "\n";
    }
}
