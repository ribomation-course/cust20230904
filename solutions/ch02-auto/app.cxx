#include <iostream>
#include <map>

auto fibonacci(int n) {
    if (n == 0) return 0UL;
    if (n == 1) return 1UL;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

struct Pair {
    int arg;
    unsigned long result;
};

auto compute(int n) {
    return Pair{.arg=n, .result=fibonacci(n)};
//    return Pair{n, fibonacci(n)};
}

auto populate(int n) {
    auto tbl = std::map<int, unsigned long>{};
    for (auto k = 1; k <= n; ++k) {
        auto [a, f] = compute(k);
        tbl[a] = f;
//        tbl[k] = fibonacci(k);
//        tbl.insert({k, fibonacci(k)});
    }
    return tbl;
}

int main() {
    auto const N = 10;
    {
        auto result = fibonacci(N);
        std::cout << "(a) fib(" << N << ") = " << result << "\n";
    }
    {
        auto [a, f] = compute(N);
        std::cout << "(b) fib(" << a << ") = " << f << "\n";
    }
    {
        for (auto [a, f]: populate(N)) {
            std::cout << "(c) fib(" << a << ") = " << f << "\n";
        }
    }
}
