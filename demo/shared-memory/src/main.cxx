#include <iostream>
#include <syncstream>
#include <thread>
#include <memory_resource>
#include <chrono>

#include <unistd.h>
#include <sys/wait.h>

#include "thread-safe-shm-memory-resource.hxx"
#include "shared-memory.hxx"
#include "message-queue.hxx"


namespace rm = ribomation::pmr;
namespace co = ribomation::concurrent;
namespace sh = ribomation::shm;
namespace mq = ribomation::mq;
namespace pmr = std::pmr;
namespace chr = std::chrono;
using std::cout;
using ElapsedType = chr::time_point<chr::system_clock, chr::nanoseconds>;

struct Message {
    unsigned long argument = 0;
    unsigned long result   = 0;
    ElapsedType   startTime;

    Message(unsigned long argument_) : argument{argument_} {
        startTime = chr::high_resolution_clock::now();
    }

    auto elapsed() const {
        auto endTime = chr::high_resolution_clock::now();
        auto elapsed = endTime - startTime;
        return chr::duration_cast<chr::microseconds>(elapsed).count();
    }

    auto elapsedUnit() const {
        return "us";
    }
};


int main(int argc, char** argv) {
    long const N = argc == 1 ? 10L : std::stol(argv[1]);

    constexpr auto MAX_MSG = 64U;
    using MQ = mq::MessageQueue<Message*, MAX_MSG>;
    cout << "sizeof(Message): " << sizeof(Message) << "\n";
    cout << "sizeof(MQ): " << sizeof(MQ) << "\n";

    constexpr auto queue_size   = 2 * sizeof(MQ);
    constexpr auto storage_size = MAX_MSG * sizeof(Message) * 10;
    constexpr auto pmr_size     = sizeof(pmr::monotonic_buffer_resource) + sizeof(rm::shm_pool_resource);
    constexpr auto shm_size     = queue_size + pmr_size + storage_size;
    cout << "queue_size  : " << queue_size << "\n";
    cout << "storage_size: " << storage_size << "\n";
    cout << "pmr_size    : " << pmr_size << "\n";
    cout << "shm_size    : " << shm_size << "\n";

    auto shm     = sh::SharedMemory{shm_size};
    auto mq1     = new(shm.allocate<MQ>()) MQ{};
    auto mq2     = new(shm.allocate<MQ>()) MQ{};
    auto storage = new(shm.allocateBytes(storage_size)) std::byte[storage_size];
    auto buffer  = new(shm.allocate<pmr::monotonic_buffer_resource>()) pmr::monotonic_buffer_resource{storage, storage_size, pmr::null_memory_resource()};
    auto pool    = new(shm.allocate<rm::shm_pool_resource>()) rm::shm_pool_resource{buffer};

    auto rc = ::fork();
    if (rc < 0) {
        cout << "fork() failed\n";
        exit(1);
    }
    if (rc == 0) {
        auto consumer = [&mq2, &pool] {
            for (auto msg = mq2->get(); msg->argument > 0L; msg = mq2->get()) {
                std::osyncstream{cout} << "[cons] <- "
                                       << msg->argument << ":" << msg->result
                                       << " (" << msg->elapsed() << msg->elapsedUnit() << ")\n";

                pool->deallocate(msg, sizeof(Message));
            }
            std::osyncstream{cout} << "[cons] done\n";
        };

        auto c = std::thread{consumer};
        c.join();

        std::osyncstream{cout} << "[cons] terminates\n";
        exit(0);
    }

    auto rc2 = ::fork();
    if (rc2 < 0) {
        cout << "fork() failed\n";
        exit(1);
    }
    if (rc2 == 0) {
        auto transformer = [&pool, &mq1, &mq2] {
            for (auto msg = mq1->get(); msg->argument > 0L; msg = mq1->get()) {
                std::osyncstream{cout} << "[trans] <- " << msg->argument << "\n";
                msg->result = msg->argument * msg->argument;
                std::osyncstream{cout} << "[trans] -> " << msg->result << "\n";
                mq2->put(msg);
            }
            mq2->put(new(pool->allocate(sizeof(Message))) Message{0});
            std::osyncstream{cout} << "[trans] done\n";
        };

        auto t = std::thread{transformer};
        t.join();

        std::osyncstream{cout} << "[trans] terminates\n";
        exit(0);
    }

    {
        auto producer = [&pool, &mq1, N] {
            for (auto k = 1U; k <= N; ++k) {
                std::osyncstream{cout} << "[prod] -> " << k << "\n";
                auto msg = new(pool->allocate(sizeof(Message))) Message{k};
                mq1->put(msg);
            }
            mq1->put(new(pool->allocate(sizeof(Message))) Message{0});

            std::osyncstream{cout} << "[prod] done\n";
        };

        auto p = std::thread{producer};
        p.join();
    }

    std::osyncstream{cout} << "[parent] waiting for child processes to terminate\n";
    ::wait(nullptr);
    ::wait(nullptr);

}
