#pragma once

#include <iostream>
#include <cstring>

namespace ribomation {
    inline char* copystr(char const* s) {
        return ::strcpy(new char[::strlen(s) + 1], s);
    }

    class Person {
        char* name;
        unsigned age;
    public:
        Person() : name{copystr("")}, age{} {
            std::cout << "Person{} @ " << this << "\n";
        }

        Person(char const* name_, unsigned age_) : name{copystr(name_)}, age{age_} {
            std::cout << "Person{" << name << ", " << age << "} @ " << this << "\n";
        }

        Person(Person const& rhs) = delete;     // (1)
        Person(Person&& rhs) noexcept: name{rhs.name}, age{rhs.age} {   // (2)
            rhs.name = nullptr;
            rhs.age = 0;
            std::cout << "Person{&& <-- " << &rhs << "} @ " << this << "\n";
        }

        ~Person() {
            delete[] name;
            std::cout << "~Person{} @ " << this << "\n";
        }

        friend auto operator<<(std::ostream& os, Person const& rhs) -> std::ostream& {
            return os << "Person{" << (rhs.name ? rhs.name : "?")   // (3)
                      << ", " << rhs.age << "} @ " << &rhs;
        }
    };
}
