#pragma once

#include <string>
#include <cstring>
#include <cerrno>
#include <stdexcept>
#include <fcntl.h>
#include <unistd.h>

namespace ribomation::io {
    using std::string;
    using namespace std::string_literals;

    class WritableFile {
        int fd;
    public:
        explicit WritableFile(string const& filename) : fd{::open(filename.c_str(), O_WRONLY | O_CREAT),} {
            if (fd == -1) {
                throw std::invalid_argument{"cannot open "s + filename + ": " + ::strerror(errno)};
            }
        }

        ~WritableFile() {
            ::close(fd);
        }

        void print(string const& txt) const {
            auto cnt = ::write(fd, txt.c_str(), txt.size());
            if (cnt == -1 /*|| cnt != txt.size()*/) {
                throw std::invalid_argument{"failed to write: "s + ::strerror(errno)};
            }
        }
    };
}
