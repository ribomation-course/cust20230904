#include <iostream>
#include <memory>
#include "person.hxx"

using namespace std::string_literals;
using std::cout;
using std::endl;
using std::unique_ptr;
using std::make_unique;
using ribomation::Person;

auto func2(unique_ptr<Person> q) -> unique_ptr<Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << endl;
    return q;
}

auto func1(unique_ptr<Person> q) -> unique_ptr<Person> {
    q->incrAge();
    cout << "[func1] q: " << *q << endl;
    return func2(std::move(q));
}

void use_unique_ptr() {
    auto anna = make_unique<Person>("Anna Conda"s, 42);
    cout << "[use_unique_ptr] anna: " << *anna << endl;

    auto p = func1(std::move(anna));

    cout << "[use_unique_ptr] p: " << *p << endl;
    cout << "[use_unique_ptr] anna: " << anna.get() << endl;
}
