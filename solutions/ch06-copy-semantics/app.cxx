#include <iostream>
#include "person.hxx"
namespace rm = ribomation;

void doit(rm::Person p) {
    std::cout << "[doit] " << p << "\n";
}

int main() {
    std::cout << "[main] --- enter ---\n";
    {
        auto anna = rm::Person{"Anna Conda", 42};
        std::cout << "[main] " << anna << "\n";
        doit(anna);
        std::cout << "[main] " << anna << "\n";
    }
    std::cout << "[main] --- mid ---\n";
    {
        rm::Person arr[5];
//        for (auto p: arr) std::cout << "[main] " << p << "\n";
//        for (auto& p: arr) std::cout << "[main] " << p << "\n";
//        for (auto const& p: arr) std::cout << "[main] " << p << "\n";
        for (auto&& p: arr) std::cout << "[main] " << p << "\n";
    }
    std::cout << "[main] --- exit ---\n";
}
