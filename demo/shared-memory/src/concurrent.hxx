#pragma once

#include <pthread.h>

namespace ribomation::concurrent {

    struct mutex {
        pthread_mutex_t m;

        mutex() {
            pthread_mutexattr_t cfg;
            pthread_mutexattr_init(&cfg);

            pthread_mutexattr_setpshared(&cfg, PTHREAD_PROCESS_SHARED);
            pthread_mutexattr_settype(&cfg, PTHREAD_MUTEX_RECURSIVE);
            pthread_mutex_init(&m, &cfg);

            pthread_mutexattr_destroy(&cfg);
        }

        ~mutex() {
            pthread_mutex_destroy(&m);
        }

        void lock() {
            pthread_mutex_lock(&m);
        }

        void unlock() {
            pthread_mutex_unlock(&m);
        }
    };

    struct unique_lock {
        mutex& m;

        unique_lock(mutex& m_) : m{m_} { m.lock(); }

        ~unique_lock() { m.unlock(); }
    };

    struct condition_variable {
        pthread_cond_t c;

        condition_variable() {
            pthread_condattr_t cfg;
            pthread_condattr_init(&cfg);

            pthread_condattr_setpshared(&cfg, PTHREAD_PROCESS_SHARED);
            pthread_cond_init(&c, &cfg);

            pthread_condattr_destroy(&cfg);
        }

        ~condition_variable() {
            pthread_cond_destroy(&c);
        }

        void wait(unique_lock& g) {
            pthread_cond_wait(&c, &g.m.m);
        }

        template<typename Predicate>
        void wait(unique_lock& g, Predicate p) {
            while (!p()) {
                wait(g);
            }
        }

        void notify_one() {
            pthread_cond_signal(&c);
        }

        void notify_all() {
            pthread_cond_broadcast(&c);
        }

    };
    

}


