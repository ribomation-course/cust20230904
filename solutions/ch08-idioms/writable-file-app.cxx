#include <iostream>
#include "writable-file.hxx"
namespace rm = ribomation::io;
using std::cout;

int main() {
    {
        auto f = rm::WritableFile{"./dummy.txt"};
//        auto f = rm::WritableFile{"/tmp/tjollahopp/dummy.txt"};
        f.print("this is the 1st line\n");
        f.print("this is the 2nd line\n");
        f.print("this is the 3rd line\n");
    }
    cout << "[main] exit\n";
}