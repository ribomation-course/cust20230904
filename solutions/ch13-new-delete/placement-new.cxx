#include <iostream>

struct SimpleData {
    int first  = 7;
    int second = 31;
    int third  = 63;

    explicit SimpleData(int value) : first(value), second(value * 2), third(value * 3) {
        std::cout << "SimpleData{" << value << "} @ " << this << "\n";
    }

    ~SimpleData() {
        std::cout << "~SimpleData() @ " << this << "\n";
    }

    friend auto operator<<(std::ostream& os, const SimpleData& d) -> std::ostream& {
        return os << "Data{" << d.first << ", " << d.second << ", " << d.third << "}";
    }
};

int main() {
    using std::cout;

    char buf[sizeof(SimpleData)];
    auto ptr = new (buf) SimpleData{10};
    cout << "*ptr: " << *ptr << " @ " << ptr << "\n";

    auto p = reinterpret_cast<int*>(ptr);
    cout << "p[0]: " << p[0] << ", " << *p << "\n";
    cout << "p[1]: " << p[1] << ", " << *(p + 1) << "\n";
    cout << "p[2]: " << p[2] << ", " << *(p + 2) << "\n";
    ptr->~SimpleData();
    //delete ptr;  //this will not work!
}
