cmake_minimum_required(VERSION 3.25)
project(ch01_initialization)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(ch01_initialization app.cxx)
target_compile_options(ch01_initialization PRIVATE ${WARN})


