#include <iostream>
#include <string>
#include <vector>
using namespace std::string_literals;
using std::cout;
using std::string;

int main() {
    std::vector<string> words = {
            "C++"s, "is"s, "really"s, "cool"s
    };
    for (string const& w : words) {
        cout << w << " ";
    }
    cout << "\n";

    string sentence = "It was a dark and silent night."s;
    if (unsigned long idx = sentence.find("and"s); idx != string::npos) {
        cout << "Found it: \"" << sentence.substr(idx, 8) << "\"\n";
    }
}
