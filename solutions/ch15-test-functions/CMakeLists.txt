cmake_minimum_required(VERSION 3.16)
project(ch15_test_functions)

set(CMAKE_CXX_STANDARD 17)

include(FetchContent)
FetchContent_Declare(gtest
        GIT_REPOSITORY  https://github.com/google/googletest.git
        GIT_TAG         main
        GIT_SHALLOW     true
)
FetchContent_MakeAvailable(gtest)

add_executable(test-app
        numeric-stats.hxx
        test-app.cxx
        )
target_link_libraries(test-app PRIVATE gtest gtest_main)

